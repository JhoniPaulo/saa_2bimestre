﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SAA.Domain
{
    public class Interesse
    {
        public int id { get; set; }

        [Required(ErrorMessage = "Título é Obrigatório")]
        [DisplayName("Título")]
        public string titulo { get; set; }
    }
}
