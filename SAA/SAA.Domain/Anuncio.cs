﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace SAA.Domain
{
    public class Anuncio
    {
        public int id { get; set; }

        [Required(ErrorMessage = "Título é Obrigatório")]
        [DisplayName("Título")]
        public string titulo { get; set; }
        public DateTime dataCriacao { get; set; }

        [DisplayName("Imagem de capa")]
        [DataType(DataType.Upload)]
        public string imagem { get; set; }

        [Required(ErrorMessage = "Descrição é Obrigatório")]
        [DisplayName("Descrição")]
        public string conteudo { get; set; }

        [Required(ErrorMessage = "Autor é Obrigatório")]
        [DisplayName("Autor")]
        public Empresa empresa { get; set; }

        [Required(ErrorMessage = "Categoria é Obrigatório")]
        [DisplayName("Categoria")]
        public Interesse interesse { get; set; }

        public Anuncio()
        {
            empresa = new Empresa();
        }
    }
}
