﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SAA.Domain
{
    public class Preferencia
    {
        public int id { get; set; }

        public Cliente cliente { get; set; }
        public Interesse interesse { get; set; }
    }
}
