﻿using System;
using System.Collections.Generic;
using SAA.Domain.CustomValidations;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.ComponentModel;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc;

namespace SAA.Domain
{
    [Bind("cpf, email, dataCadastro, idade, nome, userId")]
    public class Cliente
    {
        public int id { get; set; }

        [Required]
        [BindNever]
        public UserIdentity userIdentity { get; set; }
        
        [Required]
        public string userId { get; set; }

        [Required(ErrorMessage = "Nome é Obrigatório")]
        [DisplayName("Nome")]
        public string nome { get; set; }

        [Required(ErrorMessage = "Cpf é Obrigatório")]
        [MaxLength(14, ErrorMessage = "Cpf contém somente 11 dígitos")]
        [MinLength(11, ErrorMessage = "Cpf é composto por 11 dígitos")]
        [CpfValidation(ErrorMessage = "Digite um cpf válido")]
        [DisplayName("Cpf")]
        public string cpf { get; set; }

        [DisplayName("Idade")]
        public int idade { get; set; }

        [Required(ErrorMessage = "Email é Obrigatório")]
        //[RegularExpression
        //    (@"/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/",
        //    ErrorMessage = "Digite um email válido")
        //]
        [EmailAddress(ErrorMessage = "Digite um email válido")]
        [DisplayName("Email")]
        public string email { get; set; }

        [DisplayName("Preferências")]
        public List<Preferencia> preferencias { get; set; }

        public DateTime dataCadastro { get; set; }

        public Cliente()
        {
            preferencias = new List<Preferencia>();
        }
    }
}
