﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace SAA.Domain
{
    public class UserIdentity: IdentityUser
    {
        public Cliente cliente { get; set; }

        public Empresa empresa { get; set; }

        //public UserIdentity()
        //{
        //    cliente = new Cliente()
        //    {
        //        email = Email,
        //        dataCadastro = DateTime.Now
        //    };
        //}
    }
}
