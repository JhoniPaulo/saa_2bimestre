﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SAA.Domain.CustomValidations;

namespace SAA.Domain
{
    public class Empresa
    {
        public int id { get; set; }

        [Required]
        public UserIdentity userIdentity { get; set; }
        [Required]
        public string userId { get; set; }

        [Required(ErrorMessage = "Nome é Obrigatório")]
        [DisplayName("Nome")]
        public string nome { get; set; }

        [Required(ErrorMessage = "Email é Obrigatório")]
        //[RegularExpression
        //    (@"/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/",
        //    ErrorMessage = "Digite um email válido")
        //]
        [EmailAddress(ErrorMessage = "Digite um email válido")]
        [DisplayName("Email")]
        public string email { get; set; }

        [Required(ErrorMessage = "Cnpj é Obrigatório")]
        [MaxLength(18, ErrorMessage = "Cnpj contém somente 14 dígitos")]
        [MinLength(14, ErrorMessage = "Cnpj é composto por 14 dígitos")]
        //[CnpjValidation(ErrorMessage = "Digite um cnpj válido")]
        [DisplayName("Cnpj")]
        public string cnpj { get; set; }
        public DateTime dataCadastro { get; set; }

        [DisplayName("Anúncios")]
        public List<Anuncio> anuncios { get; set; }

        public Empresa()
        {
            anuncios = new List<Anuncio>();
        }
    }
}
