﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SAA.DAL;
using SAA.Domain;

namespace SAA.Front.Pages.Paginas.Clientes
{
    public class CreateModel : PageModel
    {
        private readonly SAA.DAL.Context _context;

        public CreateModel(SAA.DAL.Context context)
        {
            _context = context;
        }       
        
        public Cliente Cliente { get; set; }

        public IActionResult OnGet([Bind("cpf, email, dataCadastro, idade, nome, userId")] Cliente Cliente)
        {
            try
            {
                return Page();
            }
            catch (Exception e)
            {

                throw;
            }
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync([Bind("cpf, email, dataCadastro, idade, nome, userId")] Cliente Cliente)
        {
            Cliente.userIdentity = await _context.Users.FirstOrDefaultAsync(x => x.Id == Cliente.userId) ?? new UserIdentity();

            if (!ModelState.IsValid && (Cliente.userIdentity == null))
            {
                return Page();
            }            

            _context.Clientes.Add(Cliente);
            var Result = await _context.SaveChangesAsync();

            return RedirectToPage("./Details", new { id = Cliente.id });
        }
    }
}
