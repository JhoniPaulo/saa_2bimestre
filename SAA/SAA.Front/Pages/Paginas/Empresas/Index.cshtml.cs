﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using SAA.DAL;
using SAA.Domain;

namespace SAA.Front.Pages.Paginas.Empresas
{
    public class IndexModel : PageModel
    {
        private readonly SAA.DAL.Context _context;

        public IndexModel(SAA.DAL.Context context)
        {
            _context = context;
        }

        public IList<Empresa> Empresa { get;set; }

        public async Task OnGetAsync()
        {
            Empresa = await _context.Empresas
                .Include(e => e.userIdentity).ToListAsync();
        }
    }
}
