﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using SAA.DAL;
using SAA.Domain;

namespace SAA.Front.Pages.Paginas.Empresas
{
    public class DetailsModel : PageModel
    {
        private readonly SAA.DAL.Context _context;

        public DetailsModel(SAA.DAL.Context context)
        {
            _context = context;
        }

        public Empresa Empresa { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Empresa = await _context.Empresas
                .Include(e => e.userIdentity).FirstOrDefaultAsync(m => m.id == id);

            if (Empresa == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
