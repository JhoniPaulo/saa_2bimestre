﻿using SAA.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SAA.DAL
{
    public class PreferenciaDAL
    {
        static Context ctx = new Context();
        public static bool CadastrarPreferencias(List<int> interesses, string email)
        {
            var cliente = ctx.Clientes.SingleOrDefault(x => x.email == email);
            foreach (var i in interesses)
            {                
                Preferencia preferencia = new Preferencia() { 
                    cliente = cliente,
                    interesse = ctx.Interesses.SingleOrDefault(x => x.id == i)
                };

                ctx.Preferencias.Add(preferencia);
            }

            ctx.SaveChanges();
            return true;
        }

        public static bool TemPreferenciasCliente(string email)
        {
            var result = from c in ctx.Clientes
                        join p in ctx.Preferencias on c.id equals p.cliente.id
                        where c.email == email
                        select p;

            return result.Count() > 0 ? true : false;
        }

        public static List<int> ListarInteressesPorCliente(string email)
        {
            IQueryable<Interesse> interesses = from p in ctx.Preferencias
                             join c in ctx.Clientes
                             on p.cliente.id equals c.id
                             join i in ctx.Interesses
                             on p.interesse.id equals i.id
                             where c.email == email
                             select i;
            return interesses.Select(x => x.id).ToList();
        }
    }
}
