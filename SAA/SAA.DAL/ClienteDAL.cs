﻿using SAA.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SAA.DAL
{
    public class ClienteDAL
    {
        static Context ctx = new Context();
        public static Cliente CadastrarCliente(Cliente cliente)
        {
            try
            {
                cliente.dataCadastro = DateTime.Now;
                ctx.Clientes.Add(cliente);
                ctx.SaveChanges();
                return cliente;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<Cliente> ListarCliente()
        {
            try
            {
                return ctx.Clientes.Select(x => x).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
