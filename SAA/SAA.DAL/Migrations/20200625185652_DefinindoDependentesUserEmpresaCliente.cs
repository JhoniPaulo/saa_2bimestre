﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SAA.DAL.Migrations
{
    public partial class DefinindoDependentesUserEmpresaCliente : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Clientes_clienteid",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Empresas_empresaid",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_clienteid",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_empresaid",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "clienteid",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "empresaid",
                table: "AspNetUsers");

            migrationBuilder.AlterColumn<string>(
                name: "userId",
                table: "Empresas",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "userId",
                table: "Clientes",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Empresas_userId",
                table: "Empresas",
                column: "userId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Clientes_userId",
                table: "Clientes",
                column: "userId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Clientes_AspNetUsers_userId",
                table: "Clientes",
                column: "userId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Empresas_AspNetUsers_userId",
                table: "Empresas",
                column: "userId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Clientes_AspNetUsers_userId",
                table: "Clientes");

            migrationBuilder.DropForeignKey(
                name: "FK_Empresas_AspNetUsers_userId",
                table: "Empresas");

            migrationBuilder.DropIndex(
                name: "IX_Empresas_userId",
                table: "Empresas");

            migrationBuilder.DropIndex(
                name: "IX_Clientes_userId",
                table: "Clientes");

            migrationBuilder.AlterColumn<string>(
                name: "userId",
                table: "Empresas",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "userId",
                table: "Clientes",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<int>(
                name: "clienteid",
                table: "AspNetUsers",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "empresaid",
                table: "AspNetUsers",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_clienteid",
                table: "AspNetUsers",
                column: "clienteid");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_empresaid",
                table: "AspNetUsers",
                column: "empresaid");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Clientes_clienteid",
                table: "AspNetUsers",
                column: "clienteid",
                principalTable: "Clientes",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Empresas_empresaid",
                table: "AspNetUsers",
                column: "empresaid",
                principalTable: "Empresas",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
