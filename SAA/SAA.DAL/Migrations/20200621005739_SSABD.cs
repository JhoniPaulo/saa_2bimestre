﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SAA.DAL.Migrations
{
    public partial class SSABD : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Clientes",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nome = table.Column<string>(nullable: true),
                    cpf = table.Column<string>(nullable: true),
                    idade = table.Column<int>(nullable: false),
                    email = table.Column<string>(nullable: true),
                    senha = table.Column<string>(nullable: true),
                    dataCadastro = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clientes", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Empresas",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nome = table.Column<string>(nullable: true),
                    email = table.Column<string>(nullable: true),
                    cnpj = table.Column<string>(nullable: true),
                    senha = table.Column<string>(nullable: true),
                    dataCadastro = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Empresas", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Interesses",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    titulo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Interesses", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Anuncios",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    titulo = table.Column<string>(nullable: true),
                    dataCriacao = table.Column<DateTime>(nullable: false),
                    imagem = table.Column<string>(nullable: true),
                    conteudo = table.Column<string>(nullable: true),
                    empresaid = table.Column<int>(nullable: true),
                    interesseid = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Anuncios", x => x.id);
                    table.ForeignKey(
                        name: "FK_Anuncios_Empresas_empresaid",
                        column: x => x.empresaid,
                        principalTable: "Empresas",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Anuncios_Interesses_interesseid",
                        column: x => x.interesseid,
                        principalTable: "Interesses",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Preferencias",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    clienteid = table.Column<int>(nullable: true),
                    interesseid = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Preferencias", x => x.id);
                    table.ForeignKey(
                        name: "FK_Preferencias_Clientes_clienteid",
                        column: x => x.clienteid,
                        principalTable: "Clientes",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Preferencias_Interesses_interesseid",
                        column: x => x.interesseid,
                        principalTable: "Interesses",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Anuncios_empresaid",
                table: "Anuncios",
                column: "empresaid");

            migrationBuilder.CreateIndex(
                name: "IX_Anuncios_interesseid",
                table: "Anuncios",
                column: "interesseid");

            migrationBuilder.CreateIndex(
                name: "IX_Preferencias_clienteid",
                table: "Preferencias",
                column: "clienteid");

            migrationBuilder.CreateIndex(
                name: "IX_Preferencias_interesseid",
                table: "Preferencias",
                column: "interesseid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Anuncios");

            migrationBuilder.DropTable(
                name: "Preferencias");

            migrationBuilder.DropTable(
                name: "Empresas");

            migrationBuilder.DropTable(
                name: "Clientes");

            migrationBuilder.DropTable(
                name: "Interesses");
        }
    }
}
