﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SAA.DAL.Migrations
{
    public partial class CorrigindoRelacioanemntoUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "clienteid",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "empresaid",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_clienteid",
                table: "AspNetUsers",
                column: "clienteid");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_empresaid",
                table: "AspNetUsers",
                column: "empresaid");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Clientes_clienteid",
                table: "AspNetUsers",
                column: "clienteid",
                principalTable: "Clientes",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Empresas_empresaid",
                table: "AspNetUsers",
                column: "empresaid",
                principalTable: "Empresas",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Clientes_clienteid",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Empresas_empresaid",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_clienteid",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_empresaid",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "clienteid",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "empresaid",
                table: "AspNetUsers");
        }
    }
}
