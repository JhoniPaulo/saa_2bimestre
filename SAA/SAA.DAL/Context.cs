﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SAA.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace SAA.DAL
{
    public class Context: IdentityDbContext<UserIdentity>
    {
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Anuncio> Anuncios { get; set; }
        public DbSet<Empresa> Empresas { get; set; }
        public DbSet<Interesse> Interesses { get; set; }
        public DbSet<Preferencia> Preferencias { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=SAADB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            //User com Cliente
            builder.Entity<UserIdentity>()
                .HasOne(u => u.cliente)
                .WithOne(i => i.userIdentity)
                .HasForeignKey<Cliente>(c => c.userId);

            //User com Empresa
            builder.Entity<UserIdentity>()
                .HasOne(u => u.empresa)
                .WithOne(i => i.userIdentity)
                .HasForeignKey<Empresa>(c => c.userId);
        }
    }
}
