﻿using SAA.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;

namespace SAA.DAL
{
    public class InteresseDAL
    {
		static Context ctx = new Context();
        public static bool CadastrarInteresses()
        {
			try
			{
				List<Interesse> interesses = new List<Interesse>();
				interesses.Add(new Interesse() { titulo = "Geek" });
				interesses.Add(new Interesse() { titulo = "Moda" });
				interesses.Add(new Interesse() { titulo = "Automóveis" });

				foreach (var interesse in interesses)
				{
					ctx.Interesses.Add(interesse);
				}
				ctx.SaveChanges();
				return true;
			}
			catch (Exception)
			{

				throw;
			}
        }

		public static List<Interesse> ListarInteresses()
		{
			try
			{
				return ctx.Interesses.ToList();
			}
			catch (Exception)
			{

				throw;
			}
		}

		public static int? CadastrarInteresse(string nome)
		{
			try
			{
				string nomeUpper = nome.ToLower().ToUpper();
				ctx.Interesses.Add(new Interesse() { titulo = nomeUpper });
				ctx.SaveChanges();
				return ctx.Interesses.SingleOrDefault(x => x.titulo == nomeUpper).id;
			}
			catch (Exception)
			{

				throw;
			}
		}
	}
}
