﻿using SAA.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SAA.DAL
{
    public class AnuncioDAL
    {
        static Context ctx = new Context();
        public static Anuncio CadastrarAnuncio(Anuncio anuncio, string email, int? interesseId)
        {
            try
            {
                Empresa emp = new Empresa();
                emp = ctx.Empresas.SingleOrDefault(x => x.email == email);

                Interesse interesse = new Interesse();
                interesse = ctx.Interesses.SingleOrDefault(x => x.id == interesseId);

                anuncio.interesse = interesse;

                anuncio.dataCriacao = DateTime.Now;
                emp.anuncios.Add(anuncio);
                anuncio.empresa = emp;

                ctx.Anuncios.Add(anuncio);
                ctx.SaveChanges();
                return anuncio;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public static IQueryable<Anuncio> ListarAnuncios()
        {
            try
            {
                var anuncios = from em in ctx.Empresas
                               join an in ctx.Anuncios on em.id equals an.empresa.id
                               select new Anuncio()
                               {
                                   titulo = an.titulo,
                                   conteudo = an.conteudo,
                                   imagem = an.imagem,
                                   empresa = em
                               };

                return anuncios;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static IQueryable<Anuncio> ListarAnunciosPorUsuario(List<int> interessesId)
        {
            var anuncios = from a in ctx.Anuncios
                           join i in ctx.Interesses on a.interesse.id equals i.id
                           join e in ctx.Empresas on a.empresa.id equals e.id
                           where interessesId.Select(x => x).Contains(i.id)
                           select new Anuncio() { 
                            conteudo = a.conteudo,
                            imagem = a.imagem,
                            titulo = a.titulo,
                            id = a.id,
                            interesse = i,
                            empresa = e
                           };
            return anuncios;
        }

        public static IQueryable<Anuncio> ListarAnunciosPorEmpresa(string email)
        {
            try
            {
                var anuncios = from em in ctx.Empresas
                               join an in ctx.Anuncios on em.id equals an.empresa.id
                               where em.email == email
                               select new Anuncio()
                               {
                                   titulo = an.titulo,
                                   conteudo = an.conteudo,
                                   imagem = an.imagem,
                                   empresa = em
                               };

                return anuncios;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
